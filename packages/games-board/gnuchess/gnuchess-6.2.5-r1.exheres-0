# Copyright 2009-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2009 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="GNU Chess is a terminal-based chess program"
DESCRIPTION="
GNU Chess lets most modern computers play a full game of chess. It has a plain
terminal interface but supports visual interfaces such as X-Windows "xboard" and
Windows-for-PC "winboard" as well as a full 3-dimensional wooden chess-board
protocol for the Novag Chess board enabling one to be relatively free of the
computer itself.
"
HOMEPAGE="http://www.gnu.org/software/chess/"
DOWNLOADS="mirror://gnu/chess/${PNV}.tar.gz"

BUGS_TO="alip@exherbo.org"
REMOTE_IDS="freecode:${PN}"

UPSTREAM_CHANGELOG="${HOMEPAGE}/ChangeLog"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/chess_faq.html [[ description = [ The GNU Chess FAQ ] ]] [[ lang = en ]]"

PLATFORMS="~amd64 ~x86"
LICENCES="GPL-3"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-util/intltool
        sys-devel/gettext[>=0.18.3]
    build+run:
        sys-libs/readline:=
    post:
        games-board/gnuchess-book
"

DEFAULT_SRC_CONFIGURE_PARAMS=( --with-readline --hates=docdir )
DEFAULT_SRC_COMPILE_PARAMS=( AR=${AR} )

