# Copyright 2009 David Leverton <dleverton@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_DATE=20110210

require gnu [ suffix=tar.gz ]
require elisp [ with_opt=true source_directory=interface ]
require checksums [ sha1=[ ${PN}-${MY_DATE}.el=166d0012e111e0412320fcbff16b69fc8faf5429 ] ]

SUMMARY="Free program that plays the game of Go"

# from ${HOMEPAGE}
DESCRIPTION="GNU Go is a free program that plays the game of Go. GNU
Go has played thousands of games on the NNGS Go server. GNU Go is now
also playing regularly on the Legend Go Server in Taiwan, on the WING
server in Japan, and many volunteers run GNU Go clients on KGS. GNU Go
has established itself as the leading non-commercial go program in the
recent tournaments that it has taken part in."

DOWNLOADS+="
    emacs? ( http://www.emacswiki.org/emacs/download/${PN}.el -> ${PN}-${MY_DATE}.el )
"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="emacs"

DEPENDENCIES="
    build+run:
        sys-libs/ncurses
        sys-libs/readline:=

    suggestion:
        games-board/cgoban  [[ description = [ Nice X11 interface ] ]]
"

BUGS_TO="dleverton@exherbo.org"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-color
    --with-ncurses
    --with-readline
)
DEFAULT_SRC_COMPILE_PARAMS=( AR=${AR} )

src_prepare() {
    # The bundled version is out of date and does not work with emacs 23
    if option emacs; then
        edo mv interface/gnugo{-big,}-xpms.el
        edo cp "${FETCHEDDIR}/${PN}-${MY_DATE}.el" interface/${PN}.el
    fi
}

src_compile() {
    default
    elisp_src_compile
}

src_install() {
    default
    elisp_src_install
}

