# Copyright 2017-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm ; then
    SCM_cmake_modules_REPOSITORY="https://github.com/asarium/cmake-modules.git"
    SCM_librocket_REPOSITORY="https://github.com/asarium/libRocket.git"
    SCM_SECONDARY_REPOSITORIES="
        cmake_modules
        librocket
    "
    SCM_EXTERNAL_REFS="
        cmake/external/rpavlik-cmake-modules:cmake_modules
        lib/libRocket:librocket
    "
else
    CMAKE_SOURCE=${WORKBASE}/fs2open.github.com
fi

MY_PNV=${PN}_${PV//./_}

require github [ user=scp-fs2open project=fs2open.github.com release=release_${PV//./_} pnv=${MY_PNV}-source-Unix suffix=tar.gz ] \
    cmake \
    lua [ multibuild=false whitelist="5.1" ]

SUMMARY="FreeSpace2 Source Code Project"
HOMEPAGE+=" http://scp.indiegames.us/"

LICENCES="fs2_open"
SLOT="0"
MYOPTIONS="
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]]
"

RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/jansson[>=2.2]
        media-libs/SDL:2[X]
        media-libs/freetype:2
        media-libs/libpng:=
        media-libs/openal
        providers:ffmpeg? ( media/ffmpeg )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libav? ( media/libav )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DCOTIRE_DEBUG:BOOL=FALSE
    -DCOTIRE_VERBOSE:BOOL=FALSE
    -DENABLE_CLANG_TIDY:BOOL=FALSE
    -DENABLE_COTIRE:BOOL=TRUE
    -DFFMPEG_USE_PRECOMPILED:BOOL=FALSE
    -DFSO_BUILD_APPIMAGE:BOOL=FALSE
    -DFSO_BUILD_INCLUDED_LIBS:BOOL=FALSE
    -DFSO_BUILD_QTFRED:BOOL=FALSE
    -DFSO_BUILD_TESTS:BOOL=FALSE
    -DFSO_BUILD_TOOLS:BOOL=FALSE
    -DFSO_FATAL_WARNINGS:BOOL=FALSE
    -DFSO_INSTALL_DEBUG_FILES:BOOL=FALSE
    -DFSO_USE_LUAJIT:BOOL=FALSE
)

src_install() {
    if ever is_scm ; then
        dobin bin/${PN}_20_1_0_x64
    else
        dobin bin/${PN}_19_0_0_x64
    fi

    dodoc "${CMAKE_SOURCE}"/Authors.md
}

pkg_postinst() {
    elog "${PN} requires a copy of the original FreeSpace2 resources. For more information see:"
    elog "https://wiki.hard-light.net/index.php/Manually_Installing_FreeSpace_2_Open"
}

